//
//  LightControlVC.m
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//
#import <MediaPlayer/MediaPlayer.h>
#import "LightControlVC.h"
#import <QuartzCore/QuartzCore.h>
#import "Setting.h"
const static float playTime = 0.1;   // 40ms
const static float pauseTime = 0.04; // 50ms
@interface LightControlVC ()

@end

@implementation LightControlVC

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  CGRect fullScreen = [[UIScreen mainScreen] bounds];
  ((UITabBarController *)self.parentViewController).tabBar.hidden = YES;
  [[((UITabBarController *)self.parentViewController).view.subviews
      objectAtIndex:0] setFrame:fullScreen];

  self.background.frame = CGRectMake(
      self.background.frame.origin.x, self.background.frame.origin.y,
      self.background.frame.size.width,
      self.background.frame.size.height - (568 - self.view.bounds.size.height));

  UIImage *volumeBackgroundImage = [[UIImage imageNamed:@"bak_progress_gray"]
      resizableImageWithCapInsets:UIEdgeInsetsMake(9, 0, 7, 0)];
  [self.volumeView
      setMinimumTrackImage:[[UIImage imageNamed:@"bak_progress_green"]
                               resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                               9, 0, 7, 0)]
                  forState:UIControlStateNormal];
  [self.volumeView setMaximumTrackImage:volumeBackgroundImage
                               forState:UIControlStateNormal];

  [self.volumeView setThumbImage:[UIImage imageNamed:@"seek_thumb_normal"]
                        forState:UIControlStateNormal];
  self.volumeView.layer.cornerRadius = 5;

  [self setFont];

  CAGradientLayer *gradient = [CAGradientLayer layer];
  gradient.frame = self.background.bounds;
  gradient.colors =
      [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:14 / 255.0
                                                     green:14 / 255.0
                                                      blue:14 / 255.0
                                                     alpha:1] CGColor],
                                (id)[[UIColor colorWithRed:100 / 255.0
                                                     green:100 / 255.0
                                                      blue:100 / 255.0
                                                     alpha:1] CGColor],
                                nil];
  [self.background.layer insertSublayer:gradient atIndex:0];

  CAGradientLayer *gradient2 = [CAGradientLayer layer];
  gradient2.frame = self.btBackground.bounds;
  gradient2.colors =
      [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:100 / 255.0
                                                     green:100 / 255.0
                                                      blue:100 / 255.0
                                                     alpha:1] CGColor],
                                (id)[[UIColor colorWithRed:44 / 255.0
                                                     green:44 / 255.0
                                                      blue:44 / 255.0
                                                     alpha:1] CGColor],
                                nil];
  [self.btBackground.layer insertSublayer:gradient2 atIndex:0];
  [self initPopUp];
  [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                         error:nil];
}

- (void)viewWillAppear:(BOOL)animated {
  isSetVolume = NO;
  self.volumeView.value = [SETTING getVolume1];
  [self.btVolume setImage:[UIImage imageNamed:@"cb_pressed"]
                 forState:UIControlStateNormal];
  self.volumeView.enabled = NO;
  switch ([SETTING getLightID]) {
  case 1: {
    [self.btID setImage:[UIImage imageNamed:@"num1"]
               forState:UIControlStateNormal];
    break;
  }
  case 2: {
    [self.btID setImage:[UIImage imageNamed:@"num2"]
               forState:UIControlStateNormal];
    break;
  }
  case 3: {
    [self.btID setImage:[UIImage imageNamed:@"num3"]
               forState:UIControlStateNormal];
    break;
  }
  case 4: {
    [self.btID setImage:[UIImage imageNamed:@"num4"]
               forState:UIControlStateNormal];
    break;
  }
  default:
    break;
  }
}

- (void)viewDidAppear:(BOOL)animated {
  isPlaying = NO;
}

- (void)setFont {
  for (UIControl *control in [self.view subviews]) {
    if ([control isKindOfClass:[UILabel class]]) {
      UILabel *label = (UILabel *)control;
      [label setFont:[UIFont fontWithName:@"Roboto-Bold" size:14]];
        if([label.text isEqualToString:@"스마트 스위치"])
            [label setFont:[UIFont fontWithName:@"Roboto-Bold" size:18]];
    }
    for (UIControl *control2 in [control subviews]) {
      if ([control2 isKindOfClass:[UILabel class]]) {
        UILabel *label2 = (UILabel *)control2;
        [label2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:12]];
      }
    }
  }
}

- (IBAction)changeTab:(id)sender {
  self.tabBarController.selectedIndex = 1;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)CheckVolume:(id)sender {
  isSetVolume = !isSetVolume;
  self.volumeView.enabled = isSetVolume;
  if (isSetVolume) {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_checked"]
                   forState:UIControlStateNormal];
  } else {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_pressed"]
                   forState:UIControlStateNormal];
  }
}

- (IBAction)ChangeVolume:(id)sender {
  [SETTING saveVolume1:(int)self.volumeView.value];
}

- (IBAction)ChangeID:(id)sender {
  self.popup.alpha = 1 - self.popup.alpha;
}

- (IBAction)PlayOn1 {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"6";
  [self Play];
}

- (IBAction)PlayOn2 {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"8";
  [self Play];
}

- (IBAction)PlayAll {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"9";
  [self Play];
}

- (IBAction)Play30min {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"0";
  [self Play];
}

- (IBAction)Play60min {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"7";
  [self Play];
}

- (IBAction)Play90min {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"5";
  [self Play];
}

- (void)initPopUp {
  // Instantiate the nib content without any reference to it.
  NSArray *nibContents =
      [[NSBundle mainBundle] loadNibNamed:@"IDPicker" owner:nil options:nil];

  // Find the view among nib contents (not too hard assuming there is only one
  // view in it).
  self.popup = [nibContents lastObject];

  // Some hardcoded layout.
  CGSize padding = (CGSize) {0, 0};
  self.popup.frame =
      (CGRect) {padding.width, padding.height, self.popup.frame.size};

  // Add to the view hierarchy (thus retain).
  [self.view addSubview:self.popup];
  self.popup.tab = 1;
  self.popup.delegate = self;
  [self.popup refresh];
  self.popup.alpha = 0;
}

- (void)closeIDPicker {
  self.popup.alpha = 0;
  switch ([SETTING getLightID]) {
  case 1: {
    [self.btID setImage:[UIImage imageNamed:@"num1"]
               forState:UIControlStateNormal];
    break;
  }
  case 2: {
    [self.btID setImage:[UIImage imageNamed:@"num2"]
               forState:UIControlStateNormal];
    break;
  }
  case 3: {
    [self.btID setImage:[UIImage imageNamed:@"num3"]
               forState:UIControlStateNormal];
    break;
  }
  case 4: {
    [self.btID setImage:[UIImage imageNamed:@"num4"]
               forState:UIControlStateNormal];
    break;
  }
  default:
    break;
  }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)disableVolume;
{
  isSetVolume = NO;
  self.volumeView.enabled = isSetVolume;
  if (isSetVolume) {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_checked"]
                   forState:UIControlStateNormal];
  } else {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_pressed"]
                   forState:UIControlStateNormal];
  }
}

- (void)Play {
  isPlaying = YES;
  self.volumeView.enabled = NO;
  NSString *ID = [SETTING convertToSoundLight:[SETTING getLightID]];
  NSString *function = currentButton;
  // NSLog([NSString stringWithFormat:@"%@_%@", ID, function]);
  NSURL *fileURL = [[NSBundle mainBundle]
      URLForResource:[NSString stringWithFormat:@"%@_%@", ID, function]
       withExtension:@"wav"];
  NSError *error;
  self.player =
      [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
  if (!self.player)
    NSLog(@"%@", error.description);

  self.player.delegate = self;

  self.player.volume = [SETTING getVolume1] / 100.0;
  [self.player prepareToPlay];

  [self.player play];

  /*
switch (currentPosition) {
case 0:
  sound = [SETTING convertToSound:[SETTING getLightID]];
  break;
case 1:
  sound = [SETTING convertToSound:[SETTING getLightID]];
  break;

case 2:
  sound = currentButton;
  break;

case 3:
  sound = currentButton;
  break;

case 4:
  sound = [SETTING convertToSound:[SETTING getLightID]];
  break;

case 5:
  sound = currentButton;
  break;

case 6:
  sound = [SETTING convertToSound:[SETTING getLightID]];
  break;

case 7:
  sound = currentButton;
  break;

default:
  break;
}

NSURL *fileURL =
    [[NSBundle mainBundle] URLForResource:sound withExtension:@"wav"];
  if(fileURL==nil)
      fileURL =
      [[NSBundle mainBundle] URLForResource:sound withExtension:@"wav"];
NSError *error;
self.player =
    [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
if (!self.player)
  NSLog(@"%@", error.description);

self.player.delegate = self;

self.player.volume = [SETTING getVolume1] / 100.0;
[self.player prepareToPlay];

[self.player play];

currentPosition++;
[self performSelector:@selector(playNext) withObject:nil afterDelay:playTime];
   */
}

- (void)playNext {
  [self.player stop];
  if (currentPosition >= 8) {
    self.volumeView.enabled = isSetVolume;
    isPlaying = NO;
    return;
  }

  [self performSelector:@selector(Play) withObject:nil afterDelay:pauseTime];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player
                       successfully:(BOOL)flag {
  self.volumeView.enabled = isSetVolume;
  isPlaying = NO;
}
@end
