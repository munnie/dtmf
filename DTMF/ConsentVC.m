//
//  ConsentVC.m
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//

#import "ConsentVC.h"
#import "Setting.h"
const static float playTime = 0.1;   // 50ms
const static float pauseTime = 0.04; // 70ms
@interface ConsentVC ()

@end

@implementation ConsentVC

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {

  [super viewDidLoad];
  self.view.clipsToBounds = YES;
  self.background.clipsToBounds = YES;

  UIImage *volumeBackgroundImage = [[UIImage imageNamed:@"bak_progress_gray"]
      resizableImageWithCapInsets:UIEdgeInsetsMake(9, 0, 7, 0)];
  [self.volumeView
      setMinimumTrackImage:[[UIImage imageNamed:@"bak_progress_green"]
                               resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                               9, 0, 7, 0)]
                  forState:UIControlStateNormal];
  [self.volumeView setMaximumTrackImage:volumeBackgroundImage
                               forState:UIControlStateNormal];

  [self.volumeView setThumbImage:[UIImage imageNamed:@"seek_thumb_normal"]
                        forState:UIControlStateNormal];
  self.volumeView.layer.cornerRadius = 5;

  [self setFont];

  CAGradientLayer *gradient = [CAGradientLayer layer];
  if (IS_IOS_6) {
    gradient.frame = CGRectMake(0, 0, self.background.frame.size.width,
                                self.background.frame.size.height -
                                    (568 - self.view.bounds.size.height - 20));
  } else {
    gradient.frame = CGRectMake(0, 0, self.background.frame.size.width,
                                self.background.frame.size.height -
                                    (568 - self.view.bounds.size.height));
  }
  gradient.colors =
      [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:14 / 255.0
                                                     green:14 / 255.0
                                                      blue:14 / 255.0
                                                     alpha:1] CGColor],
                                (id)[[UIColor colorWithRed:100 / 255.0
                                                     green:100 / 255.0
                                                      blue:100 / 255.0
                                                     alpha:1] CGColor],
                                nil];
  [self.background.layer insertSublayer:gradient atIndex:0];

  CAGradientLayer *gradient2 = [CAGradientLayer layer];
  gradient2.frame = self.btBackground.bounds;
  gradient2.colors =
      [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:100 / 255.0
                                                     green:100 / 255.0
                                                      blue:100 / 255.0
                                                     alpha:1] CGColor],
                                (id)[[UIColor colorWithRed:44 / 255.0
                                                     green:44 / 255.0
                                                      blue:44 / 255.0
                                                     alpha:1] CGColor],
                                nil];
  [self.btBackground.layer insertSublayer:gradient2 atIndex:0];
  [self initPopUp];
  [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                         error:nil];
}

- (void)disableVolume;
{
  _isSetVolume = NO;
  self.volumeView.enabled = _isSetVolume;
  if (_isSetVolume) {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_checked"]
                   forState:UIControlStateNormal];
  } else {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_pressed"]
                   forState:UIControlStateNormal];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  _isSetVolume = NO;
  self.volumeView.value = [SETTING getVolume2];
  [self.btVolume setImage:[UIImage imageNamed:@"cb_pressed"]
                 forState:UIControlStateNormal];
  self.volumeView.enabled = NO;
  switch ([SETTING getConsentID]) {
  case 1: {
    [self.btID setImage:[UIImage imageNamed:@"num1"]
               forState:UIControlStateNormal];
    break;
  }
  case 2: {
    [self.btID setImage:[UIImage imageNamed:@"num2"]
               forState:UIControlStateNormal];
    break;
  }
  case 3: {
    [self.btID setImage:[UIImage imageNamed:@"num3"]
               forState:UIControlStateNormal];
    break;
  }
  case 4: {
    [self.btID setImage:[UIImage imageNamed:@"num4"]
               forState:UIControlStateNormal];
    break;
  }
  default:
    break;
  }
}

- (void)viewDidAppear:(BOOL)animated {
  isPlaying = NO;
  if (IS_IOS_6) {
    for (UIControl *control in [self.view subviews]) {
      for (UIControl *control1 in [control subviews]) {
        control1.frame =
            CGRectMake(control1.frame.origin.x, control1.frame.origin.y - 20,
                       control1.frame.size.width, control1.frame.size.height);
      }
    }

    self.background.frame = CGRectMake(
        self.background.frame.origin.x, self.background.frame.origin.y,
        self.background.frame.size.width,
        self.background.frame.size.height -
            (568 - self.view.bounds.size.height - 20));
  } else {
    self.background.frame = CGRectMake(
        self.background.frame.origin.x, self.background.frame.origin.y,
        self.background.frame.size.width,
        self.background.frame.size.height -
            (568 - self.view.bounds.size.height));
  }
}
- (void)setFont {
  for (UIControl *control in [self.view subviews]) {
    if ([control isKindOfClass:[UILabel class]]) {
      UILabel *label = (UILabel *)control;
      [label setFont:[UIFont fontWithName:@"Roboto-Bold" size:14]];
       // [label setFont:[UIFont fontWithName:@"Roboto-Bold" size:14]];
        if([label.text isEqualToString:@"스마트 스위치"])
            [label setFont:[UIFont fontWithName:@"Roboto-Bold" size:18]];
    }
    for (UIControl *control2 in [control subviews]) {
      if ([control2 isKindOfClass:[UILabel class]]) {
        UILabel *label2 = (UILabel *)control2;
        [label2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:14]];
          if([label2.text isEqualToString:@"스마트 스위치"])
              [label2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:18]];
      }
      for (UIControl *control3 in [control2 subviews]) {
        if ([control3 isKindOfClass:[UILabel class]]) {
          UILabel *label3 = (UILabel *)control3;
          [label3 setFont:[UIFont fontWithName:@"Roboto-Bold" size:12]];
        }
      }
    }
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)changeTab:(id)sender {
  self.tabBarController.selectedIndex = 0;
}

- (IBAction)CheckVolume:(id)sender {
  _isSetVolume = !_isSetVolume;
  self.volumeView.enabled = _isSetVolume;
  if (_isSetVolume) {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_checked"]
                   forState:UIControlStateNormal];
  } else {
    [self.btVolume setImage:[UIImage imageNamed:@"cb_pressed"]
                   forState:UIControlStateNormal];
  }
}

- (IBAction)ChangeVolume:(id)sender {
  [SETTING saveVolume2:(int)self.volumeView.value];
}

- (IBAction)ChangeID:(id)sender {
  self.popup.alpha = 1 - self.popup.alpha;
}

- (IBAction)PlayOn {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"3";
  [self Play];
}

- (IBAction)PlayOff {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"S3";
  [self Play];
}

- (IBAction)Play30min {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"2";
  [self Play];
}

- (IBAction)Play60min {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"4";
  [self Play];
}

- (IBAction)Play90min {
  if (isPlaying)
    return;
  [self.player stop];
  currentPosition = 0;
  currentButton = @"S8";
  [self Play];
}
- (void)initPopUp {
  // Instantiate the nib content without any reference to it.
  NSArray *nibContents =
      [[NSBundle mainBundle] loadNibNamed:@"IDPicker" owner:nil options:nil];

  // Find the view among nib contents (not too hard assuming there is only one
  // view in it).
  self.popup = [nibContents lastObject];

  // Some hardcoded layout.
  CGSize padding = (CGSize) {0, 0};
  self.popup.frame =
      (CGRect) {padding.width, padding.height, self.popup.frame.size};

  // Add to the view hierarchy (thus retain).
  [self.view addSubview:self.popup];
  self.popup.tab = 2;
  self.popup.delegate = self;
  [self.popup refresh];
  self.popup.alpha = 0;
}

- (void)closeIDPicker {
  self.popup.alpha = 0;
  switch ([SETTING getConsentID]) {
  case 1: {
    [self.btID setImage:[UIImage imageNamed:@"num1"]
               forState:UIControlStateNormal];
    break;
  }
  case 2: {
    [self.btID setImage:[UIImage imageNamed:@"num2"]
               forState:UIControlStateNormal];
    break;
  }
  case 3: {
    [self.btID setImage:[UIImage imageNamed:@"num3"]
               forState:UIControlStateNormal];
    break;
  }
  case 4: {
    [self.btID setImage:[UIImage imageNamed:@"num4"]
               forState:UIControlStateNormal];
    break;
  }
  default:
    break;
  }
}
- (void)Play {
  isPlaying = YES;
  self.volumeView.enabled = NO;
  NSString *ID = [SETTING convertToSoundConsent:[SETTING getConsentID]];
  NSString *function = currentButton;
  NSLog([NSString stringWithFormat:@"%@_%@", ID, function]);
  NSURL *fileURL = [[NSBundle mainBundle]
      URLForResource:[NSString stringWithFormat:@"%@_%@", ID, function]
       withExtension:@"wav"];
  NSError *error;
  self.player =
      [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
  if (!self.player)
    NSLog(@"%@", error.description);

  self.player.delegate = self;

  self.player.volume = [SETTING getVolume2] / 100.0;
  [self.player prepareToPlay];

  [self.player play];

  /*
NSString *sound;
switch (currentPosition) {
case 0:
  sound = [SETTING convertToSound:[SETTING getConsentID]];
  break;
case 1:
  sound = [SETTING convertToSound:[SETTING getConsentID]];
  break;

case 2:
  sound = currentButton;
  break;

case 3:
  sound = currentButton;
  break;

case 4:
  sound = [SETTING convertToSound:[SETTING getConsentID]];
  break;

case 5:
  sound = currentButton;
  break;

case 6:
  sound = [SETTING convertToSound:[SETTING getConsentID]];
  break;

case 7:
  sound = currentButton;
  break;

default:
  break;
}
  NSURL *fileURL =
  [[NSBundle mainBundle] URLForResource:sound withExtension:@"caf"];
  if(fileURL==nil)
      fileURL =
      [[NSBundle mainBundle] URLForResource:sound withExtension:@"wav"];
NSError *error;
self.player =
    [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
if (!self.player)
  NSLog(@"%@", error.description);

self.player.delegate = self;
self.player.volume = [SETTING getVolume2] / 100.0;
[self.player prepareToPlay];

[self.player play];
currentPosition++;

[self performSelector:@selector(playNext) withObject:nil afterDelay:playTime];
   */
}

- (void)playNext {
  [self.player stop];

  if (currentPosition >= 8) {
    isPlaying = NO;

    self.volumeView.enabled = _isSetVolume;
    return;
  }

  [self performSelector:@selector(Play) withObject:nil afterDelay:pauseTime];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player
                       successfully:(BOOL)flag {
  isPlaying = NO;
  self.volumeView.enabled = _isSetVolume;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
